library(tidyverse)
library(lubridate)
library(tis)
library(cowplot)
library(ggpubr)
library(igraph)


wd = "C:/Users/arous/Desktop/R for CA"
setwd(wd)

tab_fin<-NULL
for(nb in list.files("CA/souchepure/")){
  
for(souche in list.files(paste("CA/souchepure/",nb,"/",sep=""))){#[order(as.numeric(gsub("[^[:digit:]]","",substr(list.files("CA/souchepure/")[str_detect(list.files("CA/souchepure/"),"Desul|Xanthob")],5,6))))] # list of different reactor
  tabCA_finmean<-NULL
for(repli in list.files(paste("CA/souchepure/",nb,"/",souche,"/",sep=""))){
  tabCA_alpha=NULL
for(import in list.files(paste("CA/souchepure/",nb,"/",souche,"/",repli,"/",sep=""))) {
    
    surface_WE = 6.25
    
    #Surface of one side of the Working electrode (1cm² per sampling at end of cycle indicated) if no sampling, "#" before {} in line 36 and un-comment line ... with correct surface
    
    
    
    tab = read.delim(paste("CA/souchepure",nb,souche,repli,import,sep="/"))
    tabCA_import <- tab |> 
      mutate(temps = (mdy_hms(substr(time.s,1,nchar(time.s)-5)))) |>
      #Date format with lubridate
      mutate(I = as.numeric(substr(gsub(",",".",X.I..mA),2,7))) |> #numerical conversion of current intensity
      mutate(puiss =  10^(as.numeric(paste(substr(X.I..mA,20,20),substr(X.I..mA,23,23),sep="")))) |> 
      mutate(J = I*puiss/(surface_WE/10)) |>#Current density from current intensity and WE surface
      select(temps,I,J,puiss)|> 
      mutate(Sample=case_when(str_detect(import,"C05")&!str_detect(import,"abio")~"Xanthob1",
                              str_detect(import,"C08")&!str_detect(import,"abio")~"Xanthob2",
                              str_detect(import,"C13")&!str_detect(import,"abio")~"Desul1",
                              str_detect(import,"C15")&!str_detect(import,"abio")~"Desul2",
                              str_detect(import,"C05")&str_detect(import,"abio")~"abioXanthob1",
                              str_detect(import,"C08")&str_detect(import,"abio")~"abioXanthob2",
                              str_detect(import,"C13")&str_detect(import,"abio")~"abioDesul1",
                              str_detect(import,"C15")&str_detect(import,"abio")~"abioDesul2"))
    #Convert date format, current intensity in numeric and add current density and WE surface
    

    quantile5 = function(x,na.rm=T) {
      filtre = rep(NA, (length(x)))
      filtre[x > quantile(x,.95) | x < quantile(x,.05)] <- 0
      filtre[x <= quantile(x,.95) & x >= quantile(x,.05)] <- 1
      
      return(filtre)
      #Filter function to eliminate extrem value (quantile 5%), "#" if not necessary and in line 102
    }
    
     if (str_detect(import,"20230322")|str_detect(import,"20230424")|str_detect(import,"20230628")) {
      tabCA_alpha  <- rbind(tabCA_import,tabCA_alpha)
    } 
    
    
    if (!str_detect(import,"20230322")&!str_detect(import,"20230424")&!str_detect(import,"20230628")) {
      tabCA_alpha  <- rbind(tabCA_alpha,tabCA_import)
    }
}
   
    tabCA_outputb = tabCA_alpha|> 
      arrange(temps) |> 
      #mutate(filtre = quantile5(J)) |>
      #filter(filtre == 1) |> 
      mutate(I=as.numeric(I)*puiss) |> 
      mutate(H2 = c(0,cumsum(lintegrate(as.numeric(temps),I,xint=as.numeric(temps))/(2*96485))))
   
    tabCA_mean=tabCA_outputb |> 
     group_by(Sample) |> 
     summarize(I=mean(I))
    
    tabCA_output= tabCA_outputb |> 
   # mutate(H2bis=case_when(!str_detect(Sample,"abio")~c(0,cumsum(lintegrate(as.numeric(temps),I-tabCA_mean$I[str_detect(tabCA_mean$Sample,"abio")],xint=as.numeric(temps))/(2*96485))),
     #                      str_detect(Sample,"abio")~c(0,cumsum(lintegrate(as.numeric(temps),I,xint=as.numeric(temps))/(2*96485))))) |> 
      select(-puiss)
      
      
    #write_csv2(tabCA_output,paste(wd,"/CA/multi/",nb,"_",repli,".csv",sep =""))
 

tabCA_fin<-tabCA_output |> 
  arrange(temps) |>
  mutate(cycle=case_when(str_detect(import,"C05")~"Xanthob1",
                          str_detect(import,"C08")~"Xanthob2",
                          str_detect(import,"C13")~"Desul1",
                          str_detect(import,"C15")~"Desul2"))  |> 
  mutate(Duree=as.numeric(interval(start=temps[1],end=temps))/3600/24) 
  
  

tabCA_plot<-tabCA_fin    |> 
  nest(data=-cycle)
    
    for(batch in c(tabCA_plot$cycle)){
    
    
    pp <- ggplot(tabCA_plot$data[tabCA_plot$cycle==batch][[1]])+
      aes(Duree,J,ymin=0,ymax=1,ystep=2)+
      xlab("Temps")+
      ylab("Densité de courant |J| (A/m²)")+
      #theme(axis.text.x=element_text(size = 35),axis.text.y=element_text(size=35),plot.title=element_text(size=45),axis.title.x=element_text(size=40),axis.title.y=element_text(size=40))
      geom_smooth(color = "black",linewidth = 1,method = "gam", formula = y ~ s(x,k=30)) #courbe de regression(k = nb de points pour tracer la courde )
   # coord_cartesian(ylim=c(0,1))
     p <- ggplot(tabCA_plot$data[tabCA_plot$cycle==batch][[1]])+
      aes(Duree,J,ymin=0,ymax=0.5)+ 
      xlab("Temps")+
      ylab("Densité de courant |J| (A/m²)")+
      #theme(axis.text.x=element_text(size = 35),axis.text.y=element_text(size=35),plot.title=element_text(size=45),axis.title.x=element_text(size=40),axis.title.y=element_text(size=40))
      geom_smooth(color = "black",linewidth = 1,method = "gam", formula = y ~ s(x,k=30))+
      ggtitle(import)+
      theme_minimal(base_size=30)#+
     # coord_cartesian(xlim=c(11,14),ylim=c(0,0.5))
    
    if(str_detect(batch,"Desul")){
    plot <- pp + geom_point(aes(color=str_extract(Sample,"[^[:digit:]]+")),size = 2)+
      scale_color_manual(name="Cycle",values=c("black","black","#4472C4","#4472C4"))+
      ggtitle(batch)+
      theme_minimal(base_size=30)+
      theme(legend.position="bottom")
    plotb <- p + geom_point(aes(color=str_extract(Sample,"[^[:digit:]]+")),size = 2)+
      scale_color_manual(name="Cycle",values=c("black","black","#4472C4","#4472C4"))+
      ggtitle(batch)+
      theme_minimal(base_size=30)+
      theme(legend.position="bottom")+
      coord_cartesian(ylim=c(0,1))
    }
    
    if(str_detect(batch,"Xanth")){
    plot <- pp+ geom_point(aes(color=str_extract(Sample,"[^[:digit:]]+")),size = 2)+
      scale_color_manual(name="Cycle",values=c("black","black","#548235","#548235"))+
      ggtitle(batch)+
      theme(legend.position="bottom")+
      theme_minimal(base_size=30)
    plotb <- p+ geom_point(aes(color=str_extract(Sample,"[^[:digit:]]+")),size = 2)+
      scale_color_manual(name="Cycle",values=c("black","black","#548235","#548235"))+ 
      ggtitle(batch)+
      theme(legend.position="bottom")+
      theme_minimal(base_size=30)+
      coord_cartesian(ylim=c(0,1))
    }
    
    # ggsave(paste(wd,"/CA/graphes/",nb,"_",batch,".png",sep=""),plot = plot)
    # ggsave(paste(wd,"/CA/graphes/b/",nb,"_",batch,".png",sep=""),plot = plotb)
    # pdf(paste(wd,"/CA/graphes/",nb,"_",batch,".pdf",sep=""))
    # print(plot)
    # print(plotb)
    # dev.off()
    }
tabCA_finmean <-rbind(tabCA_finmean,tabCA_fin)
    }

  #|> 
    # mutate(Duree=round(Duree,2)) |>
    # group_by(Duree,cycle=str_extract(Sample,"[^[:digit:]]+")) |> 
    # summarize(sdmean=sd(J),Jmean=mean(J))

  
  ppmean <- ggplot(tabCA_finmean)+
    aes(Duree,J,ymin=0,ymax=1,ystep=2)+
    xlab("Temps")+
    ylab("Densité de courant |J| (A/m²)")
    #theme(axis.text.x=element_text(size = 35),axis.text.y=element_text(size=35),plot.title=element_text(size=45),axis.title.x=element_text(size=40),axis.title.y=element_text(size=40))
    #geom_smooth(color = "black",linewidth = 1,method = "gam", formula = y ~ s(x,k=30)) #courbe de regression(k = nb de points pour tracer la courde )
  #geom_ribbon(aes(ymin=Jmean-sdmean,ymax=Jmean+sdmean))
    
  if(str_detect(batch,"Desul")){
    plotmean <- ppmean + geom_line(aes(color=Sample),linewidth = 1.5)+
      scale_color_manual(name="Cycle",values=c("black","black","#4472C4","#4472C4"))+
      ggtitle(batch)+
      theme_minimal(base_size=30)+
      coord_cartesian(ylim=c(0,6))+
      theme(legend.position="bottom")}
  
  if(str_detect(batch,"Xanth")){
    plotmean <- ppmean + geom_line(aes(color=Sample),linewidth =1.5)+
      scale_color_manual(name="Cycle",values=c("black","black","#548235","#548235"))+
      ggtitle(batch)+
      theme_minimal(base_size=30)+
      coord_cartesian(ylim=c(0,6))+
      theme(legend.position="bottom")}
  ggsave(paste(wd,"/CA/graphes/",nb,"_",souche,".png",sep=""),plot = plotmean)
  pdf(paste(wd,"/CA/graphes/",nb,"_",souche,".pdf",sep=""))
  print(plotmean)
  dev.off()

  
#tab_fin<-rbind(tab_fin,mutate(rename(tabCA_finmean,Sample="cycle"),cycle=nb))
 } 
}
  completMEC<-list.files("C:/Users/arous/Desktop/R for CA/CA/multi")[str_detect(list.files("C:/Users/arous/Desktop/R for CA/CA/multi"),"Xant|Des")]
tab_complet<-NULL
for(i in completMEC){
tab_import<-read_csv2(paste("C:/Users/arous/Desktop/R for CA/CA/multi/",i,sep="")) |> 
  mutate(cycle=str_extract(i,"MEC [:digit:]|NoO2")) |> 
  mutate(Date=as.Date(temps)) |> 
  group_by(Date,Sample,cycle) |> 
  summarize(J=mean(J),H2MEC=max(H2))#,H2MEC2=max(H2bis))
tab_complet<-rbind(tab_complet,tab_import)
}
 
MEC_sp<-merge(H3Nf_souchepureMECpreplot,tab_complet,all.x=T) |> 
  pivot_longer(cols = c(DO,pH,J,H2MEC,NH4,NO3,ADN16S,nifH,bact,CO2a,CO2b,H2a,H2b,O2a,O2b,N2a,N2b),names_to="Measure",values_to="Values") |> 
  mutate(Duree=case_when(str_detect(Measure,"b")&!is.na(Values)~Duree+0.1,!str_detect(Measure,"b")~Duree)) |> 
  mutate(Measure=gsub("a","",gsub("b","",Measure))) |> 
  mutate(Measure=case_when(Measure=="ct"~"bact",
                           Measure!="ct"~Measure)) |> 
  mutate(`%CO2`=NA) |> 
  mutate(`%O2`=NA) |> 
  mutate(`%N2`=NA) |> 
  mutate(`%H2`=NA) 


for(i in levels(MEC_sp$Sample)){
  for(j in levels(as.factor(MEC_sp$cycle[MEC_sp$Sample==i]))){
    for(k in levels(as.factor(MEC_sp$t[MEC_sp$Sample==i&MEC_sp$cycle==j]))){
      for(l in levels(as.factor(MEC_sp$Duree[MEC_sp$Sample==i&MEC_sp$cycle==j&MEC_sp$t==k&!is.na(MEC_sp$Values)&MEC_sp$Measure%in%c("CO2","H2","O2","N2")]))){
        MEC_sp$`%CO2`[MEC_sp$Sample==i&MEC_sp$cycle==j&MEC_sp$t==k&MEC_sp$Duree==l&!is.na(MEC_sp$Values)&MEC_sp$Measure%in%c("CO2","H2","O2","N2")]=MEC_sp$Values[MEC_sp$Measure=="CO2"&MEC_sp$Sample==i&MEC_sp$cycle==j&MEC_sp$t==k&MEC_sp$Duree==l&!is.na(MEC_sp$Values)]/sum(MEC_sp$Values[MEC_sp$Measure%in%c("CO2","H2","O2","N2")&MEC_sp$Sample==i&MEC_sp$cycle==j&MEC_sp$t==k&MEC_sp$Duree==l&!is.na(MEC_sp$Values)])*100
        MEC_sp$`%H2`[MEC_sp$Sample==i&MEC_sp$cycle==j&MEC_sp$t==k&MEC_sp$Duree==l&!is.na(MEC_sp$Values)&MEC_sp$Measure%in%c("CO2","H2","O2","N2")]=MEC_sp$Values[MEC_sp$Measure=="H2"&MEC_sp$Sample==i&MEC_sp$cycle==j&MEC_sp$t==k&MEC_sp$Duree==l&!is.na(MEC_sp$Values)]/sum(MEC_sp$Values[MEC_sp$Measure%in%c("CO2","H2","O2","N2")&MEC_sp$Sample==i&MEC_sp$cycle==j&MEC_sp$t==k&MEC_sp$Duree==l&!is.na(MEC_sp$Values)])*100
        MEC_sp$`%O2`[MEC_sp$Sample==i&MEC_sp$cycle==j&MEC_sp$t==k&MEC_sp$Duree==l&!is.na(MEC_sp$Values)&MEC_sp$Measure%in%c("CO2","H2","O2","N2")]=MEC_sp$Values[MEC_sp$Measure=="O2"&MEC_sp$Sample==i&MEC_sp$cycle==j&MEC_sp$t==k&MEC_sp$Duree==l&!is.na(MEC_sp$Values)]/sum(MEC_sp$Values[MEC_sp$Measure%in%c("CO2","H2","O2","N2")&MEC_sp$Sample==i&MEC_sp$cycle==j&MEC_sp$t==k&MEC_sp$Duree==l&!is.na(MEC_sp$Values)])*100
        MEC_sp$`%N2`[MEC_sp$Sample==i&MEC_sp$cycle==j&MEC_sp$t==k&MEC_sp$Duree==l&!is.na(MEC_sp$Values)&MEC_sp$Measure%in%c("CO2","H2","O2","N2")]=MEC_sp$Values[MEC_sp$Measure=="N2"&MEC_sp$Sample==i&MEC_sp$cycle==j&MEC_sp$t==k&MEC_sp$Duree==l&!is.na(MEC_sp$Values)]/sum(MEC_sp$Values[MEC_sp$Measure%in%c("CO2","H2","O2","N2")&MEC_sp$Sample==i&MEC_sp$cycle==j&MEC_sp$t==k&MEC_sp$Duree==l&!is.na(MEC_sp$Values)])*100
      }
    }
  }
}
  
  plot_facile_tot<-function(tab,choix_cycle,measures){   #tab une table avec les colonnes Sample (nom des échantillons) cycle (groupe d'échantillons) Duree (time length) Measure (colonne contenant les noms de toutes les mesures effectuées) et Values (résultats des mesures présentées dans Measure)
    p<-ggplot(filter(tab,cycle%in%c(choix_cycle)&Measure%in%c(measures)&!is.na(Values)))+
      geom_line(linewidth=2)+
      geom_point(size=5)+
      theme_minimal(base_size=30)+
      guides(fill=F)+
      scale_alpha_manual(values=c(1,0.6),name="Abiotic")
    
    if(length(measures)==1){
      p<-p+aes(Duree,Values,color=str_extract(str_extract(Sample,"[^[:digit:]]+"),"[^[abio]].+"),alpha=str_detect(Sample,"abio"),fill=Sample,shape=Measure)
    }
    if(length(measures)>1){
      mes<-mean(tab$Values[tab$Measure==measures[1]&tab$cycle==choix_cycle&!is.na(tab$Values)])/mean(tab$Values[tab$Measure==measures[2]&tab$cycle==choix_cycle&!is.na(tab$Values)])
      
      if(mes>1.3){
        p<-p+aes(Duree,case_when(Measure==measures[2]~Values,Measure==measures[1]~Values/mes),color=str_extract(str_extract(Sample,"[^[:digit:]]+"),"[^[abio]].+"),alpha=str_detect(Sample,"abio"),fill=Sample,shape=Measure)+
          scale_y_continuous(name=measures[2],sec.axis = sec_axis(trans=~.*mes,name=measures[1]))
      }
      
      if(mes<0.7){
        p<-p+aes(Duree,case_when(Measure==measures[1]~Values,Measure==measures[2]~Values*mes),color=str_extract(str_extract(Sample,"[^[:digit:]]+"),"[^[abio]].+"),alpha=str_detect(Sample,"abio"),fill=Sample,shape=Measure)+
          scale_y_continuous(name=measures[1],sec.axis = sec_axis(trans=~./mes,name=measures[2]))
      }
      if(1.3>mes & mes>0.7){
        p<-p+aes(Duree,case_when(Measure==measures[1]~Values,Measure==measures[2]~Values),color=str_extract(str_extract(Sample,"[^[:digit:]]+"),"[^[abio]].+"),alpha=str_detect(Sample,"abio"),fill=Sample,shape=Measure)+
          scale_y_continuous(name=measures[1],sec.axis = sec_axis(trans=~.,name=measures[2]))
      }
    }    
    p<-p+scale_color_manual(values=c("A"="cyan","AzoDes"="cyan","Blanc"="black","Azosp"="#A9D18E","Comamo"="#ED7D31","Comamobis"="#ED7D31","Desul"="#4472C4","Xanthob"="#548235"),name="Souche")
    
    return(p)
  }

  plot_facile_tot(filter(MEC_sp,Values!=0),"MEC 2",c("bact"))+ylab("bact/L")+xlab("Durée (jours)")#+coord_cartesian(ylim=c(0,0.1)) #levels(factor(MEC_sp$cycle)) possibilité pour le deuxième argument,levels(factor(MEC_sp$Measure))~pour les possibilités du troisième argument (1 à 2 choix de mesures), MEC_sp comme table pour les MEC mais marche aussi pour souchepure

  
  
  plot_facile_mean<-function(tab,choix_cycle,measures){   #tab une table avec les colonnes Sample (nom des échantillons) cycle (groupe d'échantillons) Duree (time length) Measure (colonne contenant les noms de toutes les mesures effectuées) et Values (résultats des mesures présentées dans Measure)
    p<-ggplot(filter(tab,cycle%in%c(choix_cycle)&Measure%in%c(measures)&!is.na(Values)&Values!=0))
    
    if(length(measures)==1){
      p<-p+aes(Duree,log10(Values),fill=str_extract(str_extract(Sample,"[^[:digit:]]+"),"[^[abio]].+"),alpha=str_detect(Sample,"abio"),shape=Measure)+scale_y_continuous(breaks=10^log10(Values))
    }
    if(length(measures)>1){
      mes<-mean(tab$Values[tab$Measure==measures[1]&tab$cycle==choix_cycle&!is.na(tab$Values)])/mean(tab$Values[tab$Measure==measures[2]&tab$cycle==choix_cycle&!is.na(tab$Values)])
      
      if(mes>1.3){
        p<-p+aes(Duree,case_when(Measure==measures[2]~Values,Measure==measures[1]~Values/mes),fill=str_extract(str_extract(Sample,"[^[:digit:]]+"),"[^[abio]].+"),alpha=str_detect(Sample,"abio"),shape=Measure)+
          scale_y_continuous(name=measures[2],sec.axis = sec_axis(trans=~.*mes,name=measures[1]))
      }
      
      if(mes<0.7){
        p<-p+aes(Duree,case_when(Measure==measures[1]~Values,Measure==measures[2]~Values*mes),fill=str_extract(str_extract(Sample,"[^[:digit:]]+"),"[^[abio]].+"),alpha=str_detect(Sample,"abio"),shape=Measure)+
          scale_y_continuous(name=measures[1],sec.axis = sec_axis(trans=~./mes,name=measures[2]))
      }
      if(1.3>mes & mes>0.7){
        p<-p+aes(Duree,case_when(Measure==measures[1]~Values,Measure==measures[2]~Values),fill=str_extract(str_extract(Sample,"[^[:digit:]]+"),"[^[abio]].+"),alpha=str_detect(Sample,"abio"),shape=Measure)+
          scale_y_continuous(name=measures[1],sec.axis = sec_axis(trans=~.,name=measures[2]))
      }
      
    }    
    p<-p+stat_summary(geom="point",fun="mean",size=5,position=position_dodge2())+
      #stat_summary(geom="line",linewidth=2)+
      stat_summary(geom="errorbar",fun.data = "mean_sdl",fun.args=list(mult=1),linewidth=2,width=5,position=position_dodge2())+
      theme_minimal(base_size=30)+
      guides(color=F)+
      facet_grid(.~str_extract(str_extract(Sample,"[^[:digit:]]+"),"[^[abio]].+"))+
      scale_fill_manual(values=c("A"="cyan","AzoDes"="cyan","Blanc"="black","Azosp"="#A9D18E","Comamo"="#ED7D31","Comamobis"="#ED7D31","Desul"="#4472C4","Xanthob"="#548235"),name="Souche")+
      scale_alpha_manual(values=c(1,0.6),name="Abiotic")
    return(p)
  }
 
  plot_facile_mean(filter(MEC_sp,Values!=0&!str_detect(Sample,"abio")),"MEC 1",c("bact"))+ylab("bact/L")+xlab("Durée (jours)")+coord_cartesian(ylim=c(1e7,1e12))
  
  
  
  (MEC_sp$Values[MEC_sp$cycle=="MEC 2"&str_detect(MEC_sp$Sample,"Desul")&
                   MEC_sp$t==max(MEC_sp$t[MEC_sp$cycle=="MEC 2"])&MEC_sp$Measure=="ADN16S"&
                   !str_detect(MEC_sp$Sample,"abio")]-
      MEC_sp$Values[MEC_sp$cycle=="MEC 2"&str_detect(MEC_sp$Sample,"Desul")&
                      MEC_sp$t==0&MEC_sp$Measure=="ADN16S"&!str_detect(MEC_sp$Sample,"abio")])/
    (MEC_sp$Values[MEC_sp$cycle=="MEC 2"&str_detect(MEC_sp$Sample,"Desul")&MEC_sp$t==0&
                     MEC_sp$Measure=="ADN16S"&!str_detect(MEC_sp$Sample,"abio")])  # [ADN16S]fin-[ADN16S]0/[ADN16S]0
  
  
  1000*(MEC_sp$Values[MEC_sp$cycle=="MEC 1"&str_detect(MEC_sp$Sample,"Desul")&
                   MEC_sp$t==max(MEC_sp$t[MEC_sp$cycle=="MEC 1"])&MEC_sp$Measure=="ADN16S"&
                   !str_detect(MEC_sp$Sample,"abio")]-
      MEC_sp$Values[MEC_sp$cycle=="MEC 1"&str_detect(MEC_sp$Sample,"Desul")&
                      MEC_sp$t==0&MEC_sp$Measure=="ADN16S"&!str_detect(MEC_sp$Sample,"abio")])/
    ((MEC_sp$Duree[MEC_sp$cycle=="MEC 1"&str_detect(MEC_sp$Sample,"Desul")&
                     MEC_sp$t==max(MEC_sp$t[MEC_sp$cycle=="MEC 1"])&MEC_sp$Measure=="ADN16S"&
                     !str_detect(MEC_sp$Sample,"abio")]-MEC_sp$Duree[MEC_sp$cycle=="MEC 1"&str_detect(MEC_sp$Sample,"Desul")&MEC_sp$t==0&
                     MEC_sp$Measure=="ADN16S"&!str_detect(MEC_sp$Sample,"abio")])*5) #Bio_cath (*5 Desul ADN16S/genome, *2 Desul ADN16S/genome)

  
  
  (MEC_sp$Values[MEC_sp$cycle=="MEC 1"&str_detect(MEC_sp$Sample,"Desul")&
                                       MEC_sp$t==max(MEC_sp$t[MEC_sp$cycle=="MEC 1"])&MEC_sp$Measure=="H2"&
                                      !str_detect(MEC_sp$Sample,"abio")&!is.na(MEC_sp$Values)]-
             MEC_sp$Values[MEC_sp$cycle=="MEC 1"&str_detect(MEC_sp$Sample,"Desul")&
                                                     MEC_sp$t==0&MEC_sp$Measure=="H2"&!str_detect(MEC_sp$Sample,"abio")&!is.na(MEC_sp$Values)]) #nH2
  
  
  (MEC_sp$Values[MEC_sp$cycle=="MEC 1"&str_detect(MEC_sp$Sample,"Desul")&
                                      MEC_sp$t==max(MEC_sp$t[MEC_sp$cycle=="MEC 1"])&MEC_sp$Measure=="NH4"&
                                      !str_detect(MEC_sp$Sample,"abio")]-
             MEC_sp$Values[MEC_sp$cycle=="MEC 1"&str_detect(MEC_sp$Sample,"Desul")&
                                                     MEC_sp$t==0&MEC_sp$Measure=="NH4"&!str_detect(MEC_sp$Sample,"abio")]) #NNH4
  
  (MEC_sp$Duree[MEC_sp$cycle=="MEC 1"&str_detect(MEC_sp$Sample,"Desul")&
                                       MEC_sp$t==max(MEC_sp$t[MEC_sp$cycle=="MEC 1"])&MEC_sp$Measure=="ADN16S"&
                                       !str_detect(MEC_sp$Sample,"abio")]-MEC_sp$Duree[MEC_sp$cycle=="MEC 1"&str_detect(MEC_sp$Sample,"Desul")&MEC_sp$t==0&
                                                                                                           MEC_sp$Measure=="ADN16S"&!str_detect(MEC_sp$Sample,"abio")])#Duree
  
  
  2*(MEC_sp$Values[MEC_sp$cycle=="MEC 2"&str_detect(MEC_sp$Sample,"Desul")&
                                      MEC_sp$t==max(MEC_sp$t[MEC_sp$cycle=="MEC 2"])&MEC_sp$Measure=="H2MEC"&
                                      !str_detect(MEC_sp$Sample,"abio")]-
           MEC_sp$Values[MEC_sp$cycle=="MEC 2"&str_detect(MEC_sp$Sample,"Desul")&
                                                    MEC_sp$t==0&MEC_sp$Measure=="H2MEC"&!str_detect(MEC_sp$Sample,"abio")]) #ne

    
  
  
  
  ##CV
  
  wd = "C:/Users/arous/Desktop/R for CA/CV"
  for(nb in list.files(paste(wd,"souchepure/",sep="/"))){
    tabCV_alpha=NULL
    for(souche in list.files(paste(wd,"/souchepure/",nb,"/",sep=""))){#[order(as.numeric(gsub("[^[:digit:]]","",substr(list.files("CA/souchepure/")[str_detect(list.files("CA/souchepure/"),"Desul|Xanthob")],5,6))))] # list of different reactor
      tabCV_beta1=NULL
      test=0
      if(souche!="abio"){
      for(import in list.files(paste(wd,"/souchepure/",nb,"/",souche,"/",sep=""))) {
        surface_WE = 25
        test<-test+1
        #Surface of one side of the Working electrode (1cm² per sampling at end of cycle indicated) if no sampling, "#" before {} in line 36 and un-comment line ... with correct surface
        
        tab = read.delim(paste(wd,"/souchepure",nb,souche,import,sep="/"))
        tabCV_import <- tab |> 
          # filter(!str_detect(`X.I..mA`,"-")) |> 
          mutate(temps = (mdy_hms(substr(time.s,1,nchar(time.s)-5)))) |>
          #Date format with lubridate
          mutate(Ewe=as.numeric(gsub(",",".",Ewe.V))) |> 
          mutate(rep=as.character(as.numeric(gsub(",",".",cycle.number)))) |> 
          mutate(I = as.numeric(gsub(",",".",X.I..mA))) |> #numerical conversion of current intensity
          #mutate(puiss =  10^(as.numeric(paste(substr(tab[,2],20,20),substr(tab[,2],23,23),sep="")))) |> 
          mutate(J = I/(surface_WE/10)) |>#Current density from current intensity and WE surface
          select(temps,I,J,Ewe,rep)|> 
          mutate(tempsb=as.Date(temps)) |> 
          mutate(Sample=as.factor(paste(souche,nb,sep="_"))) |>
          nest(data=c(-rep,-tempsb,-Sample)) |> 
          mutate(type=test)
        
        #Convert date format, current intensity in numeric and add current density and WE surface
        
        
        
        #Filter function to eliminate extrem value (quantile 5%), "#" if not necessary and in line 102
        tabCV_beta1  <- rbind(tabCV_beta1,tabCV_import) |> 
          arrange(tempsb)
      }  
      }
      if(souche=="abio"){
        for(rea in list.files(paste(wd,"/souchepure/",nb,"/",souche,"/",sep=""))) {
          for(import in list.files(paste(wd,"/souchepure/",nb,"/",souche,"/",rea,"/",sep=""))) {
          surface_WE = 25
          test<-test+1
          #Surface of one side of the Working electrode (1cm² per sampling at end of cycle indicated) if no sampling, "#" before {} in line 36 and un-comment line ... with correct surface
          
          tab = read.delim(paste(wd,"souchepure",nb,souche,rea,import,sep="/"))
          tabCV_import <- tab |> 
            # filter(!str_detect(`X.I..mA`,"-")) |> 
            mutate(temps = (mdy_hms(substr(time.s,1,nchar(time.s)-5)))) |>
            #Date format with lubridate
            mutate(Ewe=as.numeric(gsub(",",".",Ewe.V))) |> 
            mutate(rep=as.character(as.numeric(gsub(",",".",cycle.number)))) |> 
            mutate(I = as.numeric(gsub(",",".",X.I..mA))) |> #numerical conversion of current intensity
            #mutate(puiss =  10^(as.numeric(paste(substr(tab[,2],20,20),substr(tab[,2],23,23),sep="")))) |> 
            mutate(J = I/(surface_WE/10)) |>#Current density from current intensity and WE surface
            select(temps,I,J,Ewe,rep)|> 
            mutate(tempsb=as.Date(temps)) |> 
            mutate(Sample=as.factor(paste(souche,nb,rea,sep="_"))) |>
            nest(data=c(-rep,-tempsb,-Sample)) |> 
            mutate(type=test)
          
          #Convert date format, current intensity in numeric and add current density and WE surface
          
          
          
          #Filter function to eliminate extrem value (quantile 5%), "#" if not necessary and in line 102
          tabCV_beta1  <- rbind(tabCV_beta1,tabCV_import) |> 
            arrange(tempsb)
        }
          }  
      }
   
      
      for(nam in levels(tabCV_beta1$Sample)){
        for(i in levels(as.factor(tabCV_beta1$rep[tabCV_beta1$Sample==nam]))){
          tabCV_beta1$rep[tabCV_beta1$Sample==nam&tabCV_beta1$rep==i&tabCV_beta1$tempsb!=min(tabCV_beta1$tempsb[tabCV_beta1$Sample==nam&tabCV_beta1$rep==i])]=as.character(as.numeric(i)+as.numeric(max(tabCV_beta1$rep[tabCV_beta1$type==tabCV_beta1$type[tabCV_beta1$Sample==nam&tabCV_beta1$rep==j]])))
        }
      }
       
     
     tabCV_beta2<-unnest(tabCV_beta1,data)   |>
        select(-tempsb,-type) |> 
        arrange(temps) 
      
      tabCV_alpha<-rbind(tabCV_alpha,tabCV_beta2)
    }  
    
    tabCV_outputb = tabCV_alpha|> 
      arrange(temps) 
    #mutate(filtre = quantile5(J)) |>
    #filter(filtre == 1) 
    
    
    #  tabCA_output= tabCA_outputb |> 
    # mutate(H2bis=case_when(!str_detect(Sample,"abio")~c(0,cumsum(lintegrate(as.numeric(temps),I-tabCA_mean$I[str_detect(tabCA_mean$Sample,"abio")],xint=as.numeric(temps))/(2*96485))),
    #                      str_detect(Sample,"abio")~c(0,cumsum(lintegrate(as.numeric(temps),I,xint=as.numeric(temps))/(2*96485))))) |> 
    #   select(-puiss)
    
    
    write_csv2(tabCV_outputb,paste(wd,"/multi/",nb,".csv",sep =""))
    
    
    tabCV_fin<-tabCV_outputb |> 
      arrange(temps) |>
      mutate(cycle=str_extract(Sample,"(?<=batch )[:digit:]"))  
    
    
    
    
   for(i in levels(tabCV_fin$Sample)) {
    
    
    pp <- ggplot(filter(tabCV_fin,Sample==i&rep==as.numeric(min(rep))+1))+
      aes(Ewe,J)+
      xlab("Ewe")+
      ylab("Densité de courant |J| (A/m²)")+
      #theme(axis.text.x=element_text(size = 35),axis.text.y=element_text(size=35),plot.title=element_text(size=45),axis.title.x=element_text(size=40),axis.title.y=element_text(size=40))
      geom_smooth(color = "black",linewidth = 1,method = "gam", formula = y ~ s(x,k=30))+ #courbe de regression(k = nb de points pour tracer la courde )
      theme_minimal(base_size=30)+
      theme(legend.position="bottom",legend.text = element_text(size=12),legend.title=element_text(size=20))+ guides(color = guide_legend(override.aes = list(size = 5)))
    p <- ggplot(filter(tabCV_fin,Sample==i&rep==max(rep[tabCV_fin$Sample==i])))+
      aes(Ewe,J)+ 
      xlab("Ewe")+
      ylab("Densité de courant |J| (A/m²)")+
      #theme(axis.text.x=element_text(size = 35),axis.text.y=element_text(size=35),plot.title=element_text(size=45),axis.title.x=element_text(size=40),axis.title.y=element_text(size=40))
      geom_smooth(color = "black",linewidth = 1,method = "gam", formula = y ~ s(x,k=30))+
      theme_minimal(base_size=30)+
      theme(legend.position="bottom",legend.text = element_text(size=12),legend.title=element_text(size=20))+ guides(color = guide_legend(override.aes = list(size = 5)))
    
    ppp <- ggplot(filter(tabCV_fin,Sample==i&rep==(as.numeric(max(rep))-1)/2))+
      aes(Ewe,J)+ 
      xlab("Ewe")+
      ylab("Densité de courant |J| (A/m²)")+
      #theme(axis.text.x=element_text(size = 35),axis.text.y=element_text(size=35),plot.title=element_text(size=45),axis.title.x=element_text(size=40),axis.title.y=element_text(size=40))
      geom_smooth(color = "black",linewidth = 1,method = "gam", formula = y ~ s(x,k=30))+
      theme_minimal(base_size=30)+
      theme(legend.position="bottom",legend.text = element_text(size=12),legend.title=element_text(size=20))+ guides(color = guide_legend(override.aes = list(size = 5)))
    
    
    plot <- pp+ geom_point(aes(size = 2))+
     # scale_color_manual(name="Reacteur",values=c("blue","green","lightblue","darkgreen"),label=c("N2-fix 1","Contrôle 1","N2-fix 2","Contrôle 2"))+
      ggtitle(paste(nb,souche,levels(as.factor(pp$data$rep)),sep="-"))+coord_cartesian(ylim=c(-0.2,0.2))
    
    plotb <- p+ geom_point(aes(size = 2))+
      #scale_color_manual(name="Reacteur",values=c("blue","green","lightblue","darkgreen"),label=c("N2-fix 1","Contrôle 1","N2-fix 2","Contrôle 2"))+
      ggtitle(paste(nb,souche,levels(as.factor(p$data$rep)),sep="-"))+coord_cartesian(ylim=c(-0.2,0.2))
    
    plotc <- ppp+ geom_point(aes(size = 2))+
      #scale_color_manual(name="Reacteur",values=c("blue","green","lightblue","darkgreen"),label=c("N2-fix 1","Contrôle 1","N2-fix 2","Contrôle 2"))+
      ggtitle(paste(nb,souche,levels(as.factor(ppp$data$rep)),sep="-"))+coord_cartesian(ylim=c(-0.2,0.2))
    
    ggsave(paste(wd,"/graphe/t0/",gsub(" ","",nb),gsub(" ","",souche),gsub("_NoO2","",i),".png",sep=""),plot = plot+
             theme_minimal(base_size=25)+theme(legend.position="bottom"))
    ggsave(paste(wd,"/graphe/tfin/",gsub(" ","",nb),gsub(" ","",souche),gsub("_NoO2","",i),".png",sep=""),plot = plotb+
             theme_minimal(base_size=25)+theme(legend.position="bottom"))
    ggsave(paste(wd,"/graphe/tmid/",gsub(" ","",nb),gsub(" ","",souche),gsub("_NoO2","",i),".png",sep=""),plot = plotc+
             theme_minimal(base_size=25)+theme(legend.position="bottom"))
    pdf(paste("C:/Users/arous/Desktop/R for CA/CV/graphe/",gsub(" ","",nb),gsub(" ","",souche),gsub("_NoO2","",i),".pdf",sep=""))
    plot<-plot+guides(color = guide_legend(override.aes = list(size = 10)))+theme(legend.position="right")
    print(plot+theme(legend.position = "none"))
    print(plotb+theme(legend.position = "none"))
    print(plotc+theme(legend.position = "none"))
    print(as_ggplot(get_legend(plot)))
    dev.off()        
  }
  }
  